<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = DB::table('staffs')->where('visible', '1')->get();
        $admin = DB::table('users')->where('visible', '1')->get();
        return view('home', ['data' => $data, 'admin' => $admin]);
    }
    public function profile()
    {
        return view('profile');
    }
}
