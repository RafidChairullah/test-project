@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Profile') }}</div>
                <table class="center" style="width:80%">
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                    </tr>
                    <tr>
                        <td>{{ Auth::user()->fname }}</th>
                        <td>{{ Auth::user()->lname }}</th>
                        <td>{{ Auth::user()->email }}</th>
                    </tr>
                  </table>
                  <div style="margin-left: auto; margin-right:auto; margin-top:20px; height:25%; width:25%">
                    <a href=""><button class="button">Edit Profile</button></a>
                    <div class="card-body">
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
