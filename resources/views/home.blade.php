@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Admins') }}</div>
                
                <div class="card-body">
                    @foreach ($admin as $item)
                    <table class="center" style="width:100%">
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Created At</th>
                            <th>Update At</th>
                            <th></th>
                        </tr>
                        <tr>
                            @if ($data == null)
                                No Staff Yet
                            @else
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->fname }}</td>
                            <td>{{ $item->lname }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->updated_at }}</td>
                            <td>{{ $item->created_at }}</td>
                            <td><button>Edit</button></td>
                            @endif
                        </tr>
                      </table>
                      <a href=""><button style="margin-top:10px; width: 100px;">Add Admin</button></a>
                    @endforeach
                </div>
            </div>

            <div class="card">
                <div class="card-header">{{ __('Users') }}</div>
                
                <div class="card-body">
                    @foreach ($data as $item)
                    <table class="center" style="width:100%">
                        <tr>
                            <th>ID</th>
                            <th>Creator ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Phone Number</th>
                            <th>Address</th>
                            <th>Created At</th>
                            <th>Update At</th>
                            <th></th>
                        </tr>
                        <tr>
                            @if ($data == null)
                                No Staff Yet
                            @else
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->admin_id }}</td>
                            <td>{{ $item->fname }}</td>
                            <td>{{ $item->lname }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->phone_num }}</td>
                            <td>{{ $item->address }}</td>
                            <td>{{ $item->gender }}</td>
                            <td>{{ $item->updated_at }}</td>
                            <td>{{ $item->created_at }}</td>
                            <td><button>Edit</button></td>
                            @endif
                        </tr>
                      </table>
                      <a href=""><button style="margin-top:10px; width: 100px;">Add User</button></a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
