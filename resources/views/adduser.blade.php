@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add User') }}</div>
                <form action = "{{ route('addstaff') }}" method = "post">
                    <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                    <table>
                    <tr>
                        <td>First Name</td>
                        <td><input type='text' name='fname' /></td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td><input type='text' name='lname' /></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><input type='text' name='email' /></td>
                    </tr>
                    <tr>
                        <td>address</td>
                        <td><input type='text' name='address' /></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td>
                        <select name="gender">
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                        </select></td>
                    </tr>

                    <tr>
                        <td colspan = '2'>
                        <input type = 'submit' value = "Add student"/>
                        </td>
                    </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
