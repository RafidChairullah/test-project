<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffs', function (Blueprint $table) {
            $table->id();
            $table->integer('admin_id');
            $table->string('fname');
            $table->string('lname');
            $table->boolean('visible');
            $table->string('email')->unique();
            $table->string('phone_num')->nullable();
            $table->string('address')->nullable();
            $table->char('gender')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffs');
    }
}
